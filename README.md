# OSPF DC Topology

This repository holds all the configurations for a standardized deployment using the following technologies:

* OSPF Unnumbered
* MLAG - Layer 2 ToR Server Redundancy
* Virtual Router Redundancy (VRR) - Layer 3 Active-Active Servers Gateway

## Quick Start

1) Clone this repo
```
git clone --recurse-submodules https://gitlab.com/igormirosh/ospf-mlag-citc.git && cd ospf-mlag-citc
```

2) Run the start-demo.sh script. Pass in --no-netq to skip loading the 8GB mem 4 vcpu NetQ box. This step may take some time depending on the host machine.
```
./start-demo.sh && cd cldemo2/simulation
OR
./start-demo.sh --no-netq && cd cldemo2/simulation
```

3) Enter the simulation by using `vagrant ssh oob-mgmt-server` to SSH into the oob-mgmt-server (the jump host)
```
vagrant ssh oob-mgmt-server
```

4) Once inside the oob-mgmt-server, cd into the automation directory
```
cd automation
```

5) Run the ansible playbook to deploy the demo to the fabric
```
ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff
```

## Demo architecture

### IPAM

Servers are configured for access vlan. The servers use a route for 10.0.0.0/8 pointing to their VRR (10.1.<VLAN#>.1).
**RACK01:**
```
Server01 - vlan10 - 10.1.10.101 | Leaf01   - SVI10 - 10.1.10.2/24 | Leaf02   - SVI10 - 10.1.10.3/24 | VRR10 - 10.1.10.1
Server02 - vlan20 - 10.1.20.102 |            SVI20 - 10.1.20.2/24 |            SVI20 - 10.1.20.3/24 | VRR20 - 10.1.20.1
Server03 - vlan30 - 10.1.30.103 |            SVI30 - 10.1.30.2/24 |            SVI30 - 10.1.30.3/24 | VRR30 - 10.1.30.1
``` 
**RACK02:** 
``` 
Server04 - vlan40 - 10.1.40.104 | Leaf03   - SVI40 - 10.1.40.2/24 | Leaf04   - SVI40 - 10.1.40.3/24 | VRR40 - 10.1.40.1
Server05 - vlan50 - 10.1.50.105 |            SVI50 - 10.1.50.2/24 |            SVI50 - 10.1.50.3/24 | VRR50 - 10.1.50.1
Server06 - vlan60 - 10.1.60.106 |            SVI60 - 10.1.60.2/24 |            SVI60 - 10.1.60.3/24 | VRR60 - 10.1.60.1
```
**RACK32:**
```
Server07 - vlan70 - 10.1.70.107 | border01 - SVI70 - 10.1.70.2/24 | border02 - SVI70 - 10.1.70.3/24 | VRR70 - 10.1.70.1
Server08 - vlan80 - 10.1.80.108 |            SVI80 - 10.1.80.2/24 |            SVI80 - 10.1.80.3/24 | VRR80 - 10.1.80.1
```

### Features

This automation repo is used to demonstrate the following features:
 * OSPF unnumbered fabric
 * MLAG Active-Active Layer 2 ToR Server Redundancy
 * Virtual Router Redundancy (VRR) Layer 3 Active-Active Servers Gateway

The configurations deployed by the automation in this demo will create a network infrastructure that creates L2 extension between racks. Any inter-VXLAN routed traffic will have to route between VXLANs on an external device. In this demo, it is the `fw`.

### PTM

```
cumulus@leaf01:mgmt-vrf:~$ sudo ptmctl -dl
------------------------------------------------------------------------------------------
port   cbl     exp            act            sysname   portID  portDescr    match   last
       status  nbr            nbr                                           on      upd
------------------------------------------------------------------------------------------
swp1   pass    server01:eth1  server01:eth1  server01  eth1    eth1         IfName   2s
swp2   pass    server02:eth1  server02:eth1  server02  eth1    eth1         IfName   2s
swp49  pass    leaf02:swp49   leaf02:swp49   leaf02    swp49   peerlink     IfName   2s
swp50  pass    leaf02:swp50   leaf02:swp50   leaf02    swp50   peerlink     IfName   2s
swp51  pass    spine01:swp1   spine01:swp1   spine01   swp1    fabric link  IfName   2s
swp52  pass    spine02:swp1   spine02:swp1   spine02   swp1    fabric link  IfName   2s
swp53  pass    spine03:swp1   spine03:swp1   spine03   swp1    fabric link  IfName   2s
swp54  pass    spine04:swp1   spine04:swp1   spine04   swp1    fabric link  IfName   2s
```

## Automation

### Ansible

Prerequisites:
- Cumulus Linux Reference Topology (cldemo2) has already been started and is running
- From a shell session on the oob-mgmt-server inside of the simulation

**Note: If you used the start-script.sh to start the simulation, the automation directory is already present on the oob-mgmt-server**

1) Clone the repo
```
git clone https://gitlab.com/igormirosh/ospf-mlag-citc.git && cd ospf-mlag-citc/automation
```

2) Test ansible
```
ansible pod1 -i inventories/pod1 -m ping
```

3) Run the ansible playbook to deploy the demo to the fabric
```
ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff
```

### Playbook Structure

The playbooks have the following important structure:
* Variables and inventories are stored in the same directory `automation/inventories/pod1/group_vars/`
* Backup configurations are stored in `automation/playbooks/configs/`
<!-- AIR:tour -->
## Validate

Once `deploy.yml` playbook deployd, verify OSPF Neighbors of `leaf01/leaf02`:
```
cumulus@leaf01:mgmt-vrf:~$ net show ospf neighbor
Neighbor ID       Pri State           Dead Time Address       Interface              RXmtL RqstL DBsmL
10.10.10.101      1 Full/DROther      33.839s 10.10.10.101    swp51:10.10.10.1         0     0     0
10.10.10.102      1 Full/DROther      33.735s 10.10.10.102    swp52:10.10.10.1         0     0     0
10.10.10.103      1 Full/DROther      36.155s 10.10.10.103    swp53:10.10.10.1         0     0     0
10.10.10.104      1 Full/DROther      36.199s 10.10.10.104    swp54:10.10.10.1         0     0     0
```

Verify OSPF Routing Table of `leaf01/leaf02`:
```
cumulus@leaf01:mgmt-vrf:~$ net show route ospf
RIB entry for ospf
==================
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

O   10.1.10.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:23
                          via 10.10.10.102, swp52 onlink, 00:14:23
                          via 10.10.10.104, swp54 onlink, 00:14:23
                          via 10.10.10.103, swp53 onlink, 00:14:23
O   10.1.20.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:23
                          via 10.10.10.102, swp52 onlink, 00:14:23
                          via 10.10.10.104, swp54 onlink, 00:14:23
                          via 10.10.10.103, swp53 onlink, 00:14:23
O   10.1.30.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:23
                          via 10.10.10.102, swp52 onlink, 00:14:23
                          via 10.10.10.104, swp54 onlink, 00:14:23
                          via 10.10.10.103, swp53 onlink, 00:14:23
O>* 10.1.40.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:26
  *                       via 10.10.10.102, swp52 onlink, 00:14:26
  *                       via 10.10.10.104, swp54 onlink, 00:14:26
  *                       via 10.10.10.103, swp53 onlink, 00:14:26
O>* 10.1.50.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:26
  *                       via 10.10.10.102, swp52 onlink, 00:14:26
  *                       via 10.10.10.104, swp54 onlink, 00:14:26
  *                       via 10.10.10.103, swp53 onlink, 00:14:26
O>* 10.1.60.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:14:26
  *                       via 10.10.10.102, swp52 onlink, 00:14:26
  *                       via 10.10.10.104, swp54 onlink, 00:14:26
  *                       via 10.10.10.103, swp53 onlink, 00:14:26
O>* 10.1.70.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:00:03
  *                       via 10.10.10.102, swp52 onlink, 00:00:03
  *                       via 10.10.10.104, swp54 onlink, 00:00:03
  *                       via 10.10.10.103, swp53 onlink, 00:00:03
O>* 10.1.80.0/24 [110/20] via 10.10.10.101, swp51 onlink, 00:00:03
  *                       via 10.10.10.102, swp52 onlink, 00:00:03
  *                       via 10.10.10.104, swp54 onlink, 00:00:03
  *                       via 10.10.10.103, swp53 onlink, 00:00:03
```

Verify OSPF Database for all links and prefixes on `leaf01/leaf02`: 
```
cumulus@leaf01:mgmt-vrf:~$ net show ospf database
OSPF Router with ID (10.10.10.1)

                Router Link States (Area 0.0.0.0)

Link ID         ADV Router      Age  Seq#       CkSum  Link count
10.10.10.1      10.10.10.1      1444 0x80000011 0xee20 4
10.10.10.2      10.10.10.2      1314 0x8000000f 0xe22c 4
10.10.10.3      10.10.10.3      1315 0x8000000e 0xd439 4
10.10.10.4      10.10.10.4      1314 0x8000000e 0xc447 4
10.10.10.101    10.10.10.101     631 0x80000013 0x5081 6
10.10.10.102    10.10.10.102     636 0x80000012 0x428e 6
10.10.10.103    10.10.10.103     453 0x8000000e 0x3a98 6
10.10.10.104    10.10.10.104     453 0x8000000e 0x2aa6 6
100.100.100.101 100.100.100.101  453 0x80000017 0xef2e 4
100.100.100.102 100.100.100.102  453 0x80000017 0xdf3c 4

                AS External Link States

Link ID         ADV Router      Age  Seq#       CkSum  Route
10.1.10.0       10.10.10.1      1444 0x80000001 0xb7d2 E2 10.1.10.0/24 [0x0]
10.1.10.0       10.10.10.2      1315 0x80000001 0xb1d7 E2 10.1.10.0/24 [0x0]
10.1.20.0       10.10.10.1      1444 0x80000001 0x4937 E2 10.1.20.0/24 [0x0]
10.1.20.0       10.10.10.2      1315 0x80000001 0x433c E2 10.1.20.0/24 [0x0]
10.1.30.0       10.10.10.1      1444 0x80000001 0xda9b E2 10.1.30.0/24 [0x0]
10.1.30.0       10.10.10.2      1315 0x80000001 0xd4a0 E2 10.1.30.0/24 [0x0]
10.1.40.0       10.10.10.3      1315 0x80000001 0x600a E2 10.1.40.0/24 [0x0]
10.1.40.0       10.10.10.4      1315 0x80000001 0x5a0f E2 10.1.40.0/24 [0x0]
10.1.50.0       10.10.10.3      1315 0x80000001 0xf16e E2 10.1.50.0/24 [0x0]
10.1.50.0       10.10.10.4      1315 0x80000001 0xeb73 E2 10.1.50.0/24 [0x0]
10.1.60.0       10.10.10.3      1315 0x80000001 0x83d2 E2 10.1.60.0/24 [0x0]
10.1.60.0       10.10.10.4      1315 0x80000001 0x7dd7 E2 10.1.60.0/24 [0x0]
10.1.70.0       100.100.100.101  453 0x80000007 0x4292 E2 10.1.70.0/24 [0x0]
10.1.70.0       100.100.100.102  453 0x80000006 0x3e96 E2 10.1.70.0/24 [0x0]
10.1.80.0       100.100.100.101  453 0x80000009 0xcff8 E2 10.1.80.0/24 [0x0]
10.1.80.0       100.100.100.102  453 0x80000007 0xcdfb E2 10.1.80.0/24 [0x0]
```

### Server to Server connectivity

Log into `server01`:
```
cumulus@oob-mgmt-server:~$ ssh server01
Last login: Mon Jul 13 06:51:14 2020 from 192.168.200.1
cumulus@server01:~$
```

Ping each `server` from `server01` `uplink` bond interface to validate connectivity:
```
cumulus@server01:~$ ping 10.1.30.103 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.30.103: icmp_seq=1 ttl=63 time=1.55 ms
64 bytes from 10.1.30.103: icmp_seq=2 ttl=63 time=2.08 ms
64 bytes from 10.1.30.103: icmp_seq=3 ttl=63 time=1.25 ms

cumulus@server01:~$ ping 10.1.40.104 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.40.104: icmp_seq=1 ttl=61 time=5.70 ms
64 bytes from 10.1.40.104: icmp_seq=2 ttl=61 time=3.06 ms
64 bytes from 10.1.40.104: icmp_seq=3 ttl=61 time=2.85 ms

cumulus@server01:~$ ping 10.1.50.105 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.50.105: icmp_seq=1 ttl=61 time=3.72 ms
64 bytes from 10.1.50.105: icmp_seq=2 ttl=61 time=3.51 ms
64 bytes from 10.1.50.105: icmp_seq=3 ttl=61 time=2.99 ms

cumulus@server01:~$ ping 10.1.60.106 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.60.106: icmp_seq=1 ttl=61 time=8.16 ms
64 bytes from 10.1.60.106: icmp_seq=2 ttl=61 time=14.6 ms
64 bytes from 10.1.60.106: icmp_seq=3 ttl=61 time=2.55 ms

cumulus@server01:~$ ping 10.1.70.107 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.70.107: icmp_seq=1 ttl=61 time=4.05 ms
64 bytes from 10.1.70.107: icmp_seq=2 ttl=61 time=2.77 ms
64 bytes from 10.1.70.107: icmp_seq=3 ttl=61 time=2.43 ms

cumulus@server01:~$ ping 10.1.80.108 -I 10.1.10.101 -c 3 | grep icmp
64 bytes from 10.1.80.108: icmp_seq=1 ttl=61 time=3.72 ms
64 bytes from 10.1.80.108: icmp_seq=2 ttl=61 time=2.68 ms
64 bytes from 10.1.80.108: icmp_seq=3 ttl=61 time=2.78 ms
```

<!-- AIR:tour -->
## Advanced Options

### Vagrant
How to TURN UP the Vagrant environment
```
vagrant up oob-mgmt-server oob-mgmt-switch
vagrant up /^leaf*/ /^spine*/ /^service*/ /^server*/
```
How to DESTROY the Vagrant environment
```
vagrant destroy -f /^leaf*/ /^spine*/ /^service*/ /^server*/
vagrant destroy -f oob-mgm-server oob-mgmt-switch
```

### Git
Clone repo with submodule
```
git clone --recursive https://gitlab.com/cumulus-consulting/goldenturtle/dc_configs_vxlan_evpnl2only.git
```
Fetch submodule
```
git submodule update --init
```
